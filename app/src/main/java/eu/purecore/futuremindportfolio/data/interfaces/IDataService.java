package eu.purecore.futuremindportfolio.data.interfaces;

import eu.purecore.futuremindportfolio.data.Data;
import retrofit.http.GET;
import rx.Observable;

/**
 * Company: Pure Core (office@purecore.eu)
 * Created by Jackq on 2016-07-04.
 * e-mail: jaszczypek@purecore.eu
 */
public interface IDataService {
    //http://pinky.futuremind.com/~dpaluch/test35/
    @GET("/~dpaluch/test35")
    Observable<Data> getData();
}

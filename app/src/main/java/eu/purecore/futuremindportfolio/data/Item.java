package eu.purecore.futuremindportfolio.data;

import android.support.annotation.NonNull;

import com.google.gson.annotations.SerializedName;

import java.util.Date;

/**
 * Company: Pure Core (office@purecore.eu)
 * Created by Jackq on 2016-07-04.
 * e-mail: jaszczypek@purecore.eu
 */
public class Item implements Comparable<Item> {

    @SerializedName("orderId")
    private int orderId;

    @SerializedName("title")
    private String title;

    @SerializedName("description")
    private String description;

    @SerializedName("modificationDate")
    private Date modificationDate;

    @SerializedName("image_url")
    private String image_url;

    private transient String detailsUrl;

    public Item() {
    }

    public Item(int orderId, String title, String description, Date modificationDate, String image_url, String detailsUrl) {
        this.orderId = orderId;
        this.title = title;
        this.description = description;
        this.modificationDate = modificationDate;
        this.image_url = image_url;
        this.detailsUrl = detailsUrl;
    }

    public int getOrderId() {
        return orderId;
    }

    public void setOrderId(int orderId) {
        this.orderId = orderId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getModificationDate() {
        return modificationDate;
    }

    public void setModificationDate(Date modificationDate) {
        this.modificationDate = modificationDate;
    }

    public String getImage_url() {
        return image_url;
    }

    public void setImage_url(String image_url) {
        this.image_url = image_url;
    }

    public String getDetailsUrl() {
        return detailsUrl;
    }

    public void setDetailsUrl(String detailsUrl) {
        this.detailsUrl = detailsUrl;
    }

    @Override
    public int compareTo(@NonNull Item another) {
        if (this.orderId == another.orderId)
            return 0;
        return this.orderId < another.orderId ? -1 : 1;
    }

    @Override
    public String toString() {
        return "Item{" +
                "orderId=" + orderId +
                ", title='" + title + '\'' +
                ", description='" + description + '\'' +
                ", modificationDate=" + modificationDate +
                ", image_url='" + image_url + '\'' +
                ", detailsUrl='" + detailsUrl + '\'' +
                '}';
    }
}

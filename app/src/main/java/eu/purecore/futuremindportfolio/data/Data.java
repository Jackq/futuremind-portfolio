package eu.purecore.futuremindportfolio.data;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Company: Pure Core (office@purecore.eu)
 * Created by Jackq on 2016-07-04.
 * e-mail: jaszczypek@purecore.eu
 */
public class Data {

    @SerializedName("data")
    private List<Item> itemList;

    public Data() {
        this.itemList = new ArrayList<>();
    }

    public List<Item> getItemList() {
        return itemList;
    }

    public void setItemList(List<Item> itemList) {
        this.itemList = itemList;
    }

    @Override
    public String toString() {
        String help = "";
        for (Item f : this.itemList) {
            help += f.toString();
        }
        return "Data{" +
                "item=" + help +
                '}';
    }
}

package eu.purecore.futuremindportfolio.adapters.viewholders;

import android.support.v7.widget.CardView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import butterknife.Bind;
import eu.purecore.futuremindportfolio.R;
import eu.purecore.libs.adapters.viewholders.abstracts.CustomViewHolder;

/**
 * Company: Pure Core (office@purecore.eu)
 * Created by Jackq on 2016-07-04.
 * e-mail: jaszczypek@purecore.eu
 */
public class ItemCVH extends CustomViewHolder {
    @Bind(R.id.recycler_item_title) TextView titleTextView;
    @Bind(R.id.recycler_item_modification_date) TextView modificationDateTextView;
    @Bind(R.id.recycler_item_description) TextView descriptionTextView;
    @Bind(R.id.recycler_item_image) ImageView imageView;
    @Bind(R.id.recycler_item_cart_view) CardView cardView;

    public ItemCVH(View itemView) {
        super(itemView);
    }

    public TextView getTitleTextView() {
        return titleTextView;
    }

    public TextView getModificationDateTextView() {
        return modificationDateTextView;
    }

    public TextView getDescriptionTextView() {
        return descriptionTextView;
    }

    public ImageView getImageView() {
        return imageView;
    }

    public CardView getCardView() {
        return cardView;
    }

    @Override
    public void setOnItemClikListener(View.OnClickListener onItemClikListener) {
        this.getCardView().setOnClickListener(onItemClikListener);
    }
}

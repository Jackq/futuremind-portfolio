package eu.purecore.futuremindportfolio.utils;

import com.google.gson.GsonBuilder;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.logging.HttpLoggingInterceptor;

import retrofit.GsonConverterFactory;
import retrofit.Retrofit;
import retrofit.RxJavaCallAdapterFactory;

/**
 * Company: Pure Core (office@purecore.eu)
 * Created by Jackq on 2016-07-04.
 * e-mail: jaszczypek@purecore.eu
 */
public class Core {
    public static final String DATE_FORMAT = "yyyy-MM-dd";

    private static Core instance;
    private Retrofit retrofit;

    private Core(){
        retrofit = null;
    }

    public synchronized void initRetrofit(Boolean withLog){
        if (withLog) {
            OkHttpClient client = new OkHttpClient();
            HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
            interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
            client.interceptors().add(interceptor);
            retrofit = new Retrofit.Builder()
                    .baseUrl(UserUtil.API_URL)
                    .client(client)
                    .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                    .addConverterFactory( GsonConverterFactory.create(
                            new GsonBuilder()
                                    .setDateFormat(Core.DATE_FORMAT)
                                    .create()
                            )
                    )
                    .build();
        }else {
            retrofit = new Retrofit.Builder()
                    .baseUrl(UserUtil.API_URL)
                    .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create(
                            new GsonBuilder()
                                    .setDateFormat(Core.DATE_FORMAT)
                                    .create()
                            )
                    )
                    .build();
        }
    }

    public Retrofit getRetrofit(){
        if (retrofit == null) throw new ExceptionInInitializerError("Please initialize Retrofit -> Core.get().initRetrofit([true/false] log)");
        return retrofit;
    }

    public static synchronized void init(){
        instance = new Core();
    }
    public static synchronized Core get(){
        if (instance ==null)  throw new ExceptionInInitializerError("Please initialize Core -> Core.init()");
        return instance;
    }
    public static synchronized void destroy(){
        instance = null;
    }
}

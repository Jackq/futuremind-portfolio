package eu.purecore.futuremindportfolio.utils;

import android.app.Activity;
import android.provider.Settings;

/**
 * Company: Pure Core (office@purecore.eu)
 * Created by Jackq on 2015-04-22.
 * e-mail: jaszczypek@purecore.eu
 */
public class UserUtil {
    /**
     * Zwraca unikale ID urzadzenia dla uruchomionego uzytkownika wiecej info nizej:
     * "Added in API level 3
     * A 64-bit number (as a hex string) that is randomly generated when the user first sets up the device and should remain constant for the lifetime of the user's device. The value may change if a factory reset is performed on the device.
     * Note: When a device has multiple users (available on certain devices running Android 4.2 or higher), each user appears as a completely separate device, so the ANDROID_ID value is unique to each user.
     * Constant Value: "android_id" "
     * @param This - dowolne activity
     * @return - unikatle ID
     * TODO: złangielszczyć
     */
    public static String getUniqueID(Activity This){
        return Settings.Secure.getString(This.getApplicationContext().getContentResolver(), Settings.Secure.ANDROID_ID);
    }

    /**
     * TODO: wstawic adres do api
     */
    public static final String API_URL = "http://pinky.futuremind.com";
}

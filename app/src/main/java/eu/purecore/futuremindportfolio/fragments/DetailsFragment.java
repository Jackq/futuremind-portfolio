package eu.purecore.futuremindportfolio.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;

import butterknife.Bind;
import butterknife.ButterKnife;
import eu.purecore.futuremindportfolio.R;

/**
 * Company: Pure Core (office@purecore.eu)
 * Created by Jackq on 2016-07-04.
 * e-mail: jaszczypek@purecore.eu
 */
public class DetailsFragment extends Fragment {
    private static final String URL_PARAM = "DetailsFragment_URL_param";

    private String urlDetails;
    @Bind(R.id.fragment_details_web_view) WebView webView;

    public DetailsFragment() {
        // Required empty public constructor
    }

    public static DetailsFragment newInstance(String url) {
        DetailsFragment fragment = new DetailsFragment();
        Bundle args = new Bundle();
        args.putString(DetailsFragment.URL_PARAM, url);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            this.urlDetails = getArguments().getString(DetailsFragment.URL_PARAM);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_details, container, false);

        ButterKnife.bind(this, view);
        webView.loadUrl(this.urlDetails);

        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        ButterKnife.unbind(this);
        super.onDetach();
    }
}

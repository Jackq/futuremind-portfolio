package eu.purecore.futuremindportfolio.fragments;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import butterknife.Bind;
import butterknife.ButterKnife;
import eu.purecore.futuremindportfolio.R;
import eu.purecore.futuremindportfolio.adapters.viewholders.ItemCVH;
import eu.purecore.futuremindportfolio.data.Data;
import eu.purecore.futuremindportfolio.data.Item;
import eu.purecore.futuremindportfolio.data.interfaces.IDataService;
import eu.purecore.futuremindportfolio.utils.Core;
import eu.purecore.libs.adapters.RecyclerViewAdapter;
import eu.purecore.libs.adapters.listeners.IBinder;
import eu.purecore.libs.adapters.listeners.IOnItemClickListener;
import rx.SingleSubscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

/**
 * Company: Pure Core (office@purecore.eu)
 * Created by Jackq on 2016-07-04.
 * e-mail: jaszczypek@purecore.eu
 */
public class ListFragment extends Fragment {
    private static final String SPLIT_CODE = "    ";

    @Bind(R.id.fragment_list_swipeContainer) SwipeRefreshLayout swipeRefreshLayout;
    @Bind(R.id.fragment_list_list) RecyclerView recyclerView;
    @Bind(R.id.fragment_list_progres) ProgressBar progressBar;

    private CompositeSubscription compositeSubscription;
    private RecyclerViewAdapter<Item, ItemCVH> adapter;
    private IInteractionListener interactionListener;

    public interface IInteractionListener {
        void onErrorDataLoadingAppeared(Throwable t);
        void onItemClick(Item item);
    }

    public ListFragment() {
        // Required empty public constructor
    }

    public static ListFragment newInstance() {
        return new ListFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v =inflater.inflate(R.layout.fragment_list, container, false);
        ButterKnife.bind(this, v);
        Core.init();
        Core.get().initRetrofit(true);
        this.setUpAdapter();
        this.hideContent(true);
        this.compositeSubscription = new CompositeSubscription(
                this.showData()
        );
        this.setUpRecycler();
        this.setUpSwipe();


        return v;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof IInteractionListener) {
            this.interactionListener = (IInteractionListener) context;
        } else {
            throw new RuntimeException("[" + context.toString() + "] must implement IInteractionListener. Please repair it. :)");
        }
    }

    @Override
    public void onDetach() {
        ButterKnife.unbind(this);
        if (compositeSubscription!=null) compositeSubscription.unsubscribe();
        super.onDetach();
    }
    //<editor-fold desc="REST Requests" defaultstate="collapsed">
    public Subscription showData() {
        return Core.get().getRetrofit().create(IDataService.class)
                .getData()
                .toSingle()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleSubscriber<Data>() {
                               @Override
                               public void onSuccess(Data value) {
                                   adapter.clear();
                                   for(Item i: value.getItemList()){
                                      String [] descUrl = i.getDescription().split(ListFragment.SPLIT_CODE);
                                       i.setDescription(descUrl[0]);
                                       i.setDetailsUrl(descUrl[1]);
                                   }
                                   Collections.sort(value.getItemList());//sort by "orderId"
                                   adapter.addAll(value.getItemList());
                                   hideContent(false);//show
                                   swipeRefreshLayout.setRefreshing(false);
                               }

                               @Override
                               public void onError(Throwable error) {
                                   //error.printStackTrace();
                                   hideContent(false);//show
                                   onErrorDataLoadingAppeared(error); //send info to activity
//                                   //TODO: remove!!!
//                                   adapter.clear();
//                                   List<Item> itemList = new ArrayList<>();
//                                   itemList.add(new Item(0, "tytul1", "opis", new Date(), "http://", "http://onet.pl"));
//                                   itemList.add(new Item(2, "tytul2", "opis2", new Date(), "http://", "http://onet.pl"));
//                                   itemList.add(new Item(1, "tytul1", "opis1", new Date(), "http://", "http://onet.pl"));
//                                   itemList.add(new Item(3, "tytul3", "opis3", new Date(), "http://", "http://onet.pl"));
//                                   Collections.sort(itemList);
//                                   adapter.addAll(itemList);
                                   swipeRefreshLayout.setRefreshing(false);
                               }
                           }
                );
    }
    //</editor-fold>
    //<editor-fold desc="Action implementation" defaultstate="collapsed">
    public void onErrorDataLoadingAppeared(Throwable t) {
        if (this.interactionListener != null) {
            this.interactionListener.onErrorDataLoadingAppeared(t);
        }
    }
    public void onItemClickAppeared(Item item){
        if (this.interactionListener !=null){
            this.interactionListener.onItemClick(item);
        }
    }
    //</editor-fold>
    public void hideContent(Boolean hide) {
        if (hide) {
            this.progressBar.setVisibility(View.VISIBLE);
            this.swipeRefreshLayout.setVisibility(View.GONE);
        } else {
            this.progressBar.setVisibility(View.GONE);
            this.swipeRefreshLayout.setVisibility(View.VISIBLE);
        }
    }

    private void setUpAdapter(){
        this.adapter = new RecyclerViewAdapter<>(
                getActivity(),
                ItemCVH.class,
                R.layout.recycler_item_layout,
                new IBinder<Item, ItemCVH>() {
                    @Override
                    public void bindHolderToData(ItemCVH holder, Item data, int position) {
                        holder.getModificationDateTextView().setText(new SimpleDateFormat(Core.DATE_FORMAT, Locale.getDefault())
                                .format(data.getModificationDate()));
                        holder.getTitleTextView().setText(data.getTitle());
                        holder.getDescriptionTextView().setText(data.getDescription());
                        //System.out.println(data.getImage_url());
                        //image
                        Picasso.with(getActivity())
                                .load(data.getImage_url())
                                .placeholder(R.mipmap.ic_launcher)
                                .error(R.mipmap.load_error)
                                .into(holder.getImageView());
                    }
                }
        );
        //click listenner
        this.adapter.setOnItemClickListener(new IOnItemClickListener<Item>() {
            @Override
            public void onItemClick(Item data) {
                onItemClickAppeared(data);
            }
        });
    }
    private void setUpRecycler() {
        this.recyclerView.setLayoutManager(new LinearLayoutManager(this.getActivity()));
        this.recyclerView.setHasFixedSize(true);
        this.recyclerView.setAdapter(this.adapter);
        this.recyclerView.setItemAnimator(new DefaultItemAnimator());
        // this.recyclerView.addItemDecoration(new DividerItemDecoration);
    }

    private void setUpSwipe(){
        this.swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            private Subscription run;
            @Override
            public void onRefresh() {
                if (run!=null) {
                    compositeSubscription.remove(run);
                }
                swipeRefreshLayout.setRefreshing(true);
                run = showData();
                compositeSubscription.add(run);
            }
        });
        this.swipeRefreshLayout.setColorSchemeColors(Color.BLUE, Color.WHITE);
    }
}

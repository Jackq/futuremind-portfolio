package eu.purecore.futuremindportfolio;

import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import butterknife.Bind;
import butterknife.ButterKnife;
import eu.purecore.futuremindportfolio.data.Item;
import eu.purecore.futuremindportfolio.fragments.DetailsFragment;
import eu.purecore.futuremindportfolio.fragments.ListFragment;

public class MainActivity extends AppCompatActivity implements
        ListFragment.IInteractionListener {

    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Bind(R.id.toolbar_title)
    TextView toolbarTittleTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ButterKnife.bind(this);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayShowTitleEnabled(false);
        this.setUpToolBar(this.toolbar);
        this.toolbarTittleTextView.setText(getResources().getText(R.string.main_activity_toolbar_title));

        replaceFragment(ListFragment.newInstance(), R.id.fragmentContainer, false);
    }

    @Override
    protected void onDestroy() {
        ButterKnife.unbind(this);
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        defaultSetUp();
    }

    @Override
    public void onErrorDataLoadingAppeared(Throwable t) {
        Toast.makeText(this, R.string.fragment_list_data_error_text, Toast.LENGTH_SHORT).show();
        //poor log
        t.printStackTrace();
    }

    @Override
    public void onItemClick(Item item) {
        int containerId;
        if (findViewById(R.id.fragmentContainer2) != null) { //sw600dp mode
            containerId = R.id.fragmentContainer2;
        } else {
            containerId = R.id.fragmentContainer;
            this.toolbarTittleTextView.setText(item.getTitle());
            Drawable drawable = getResources().getDrawable(R.drawable.ic_arrow_back_black_24dp);
            if (drawable != null) drawable.setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_IN);
            this.toolbar.setNavigationIcon(drawable);
            this.toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    getSupportFragmentManager().popBackStack();
                    defaultSetUp();
                }
            });
        }
        replaceFragment(DetailsFragment.newInstance(item.getDetailsUrl()), containerId, true);
    }

    private void setUpToolBar(Toolbar t) {
        t.setNavigationIcon(null);
        t.setTitle(null);
    }

    private void replaceFragment(Fragment fragment, int resource, boolean backStack) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.setCustomAnimations(R.anim.slide_in, R.anim.slide_out2, R.anim.slide_in2, R.anim.slide_out);
        transaction.replace(resource, fragment);
        if (backStack) transaction.addToBackStack(null);
        transaction.commit();
    }

    private void defaultSetUp(){
        toolbar.setNavigationIcon(null);
        toolbarTittleTextView.setText(getResources().getString(R.string.main_activity_toolbar_title));
    }
}
